# TP5 - TCP, UDP et services réseau

## 1. First steps

🌞 Déterminez, pour ces 5 applications, si c'est du TCP ou de l'UDP

__Spotify :__
TCP
>IP : 35.186.224.25
>
>Port serveur : 443
>
>Port local : 54113

__Steam :__
TCP
>IP : 172.64.145.151
>
>Port serveur : 443
>
>Port local : 54037

__Discord :__
TCP
>IP : 162.159.138.232
>
>Port serveur : 443
>
>Port local : 54144

__Youtube :__
TCP
>IP : 172.217.18.206
>
>Port serveur : 443
>
>Port local : 54227

__Agar.io :__
TCP
>IP : 104.17.234.168
>
>Port serveur : 443
>
>Port local : 54351

---
🌞 Demandez l'avis à votre OS

> netstat -a -n -b 

```
 [Discord.exe]
  TCP    10.33.51.155:54539     162.159.138.252:443    ESTABLISHED
---
 [Spotify.exe]
  TCP    10.33.51.155:54565     35.186.224.25:443      ESTABLISHED
---
[steamwebhelper.exe]
  TCP    10.33.51.155:54623     172.64.145.151:443     ESTABLISHED
---
 [chrome.exe] (agar.io)
  TCP    10.33.51.155:54671     104.17.234.168:443     ESTABLISHED
```

## 2. Setup Virtuel

### 1. SSH
---

🌞 Examinez le trafic dans Wireshark

__Voir capture :__ tp5_3_way

- __déterminez si SSH utilise TCP ou UDP__

> Transmission Control Protocol, Src Port: 58078, Dst Port: 22, Seq: 0, Len: 0

- __repérez le 3-Way Handshake à l'établissement de la connexion__

> Les 3 premières frames avec les flags SYN/SYNACK/ACK

- __repérez du trafic SSH__

> Les frames avec le protocol SSHv2

- __repérez le FIN ACK à la fin d'une connexion__

> les deux avants dernières frames avec le flag FIN ACK

---
🌞 Demandez aux OS

De ma machine :
```
  Proto  Adresse locale         Adresse distante       État
  TCP    10.5.1.1:59841         10.5.1.11:ssh          ESTABLISHED
```

De la vm :
```
tcp      ESTAB    0          0                                 10.5.1.11:ssh               10.5.1.1:59841
```

### 2. Routage
---
🌞 Prouvez que

```powershell
[nono@node1 ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=112 time=21.7 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=112 time=18.5 ms
64 bytes from 8.8.8.8: icmp_seq=3 ttl=112 time=19.3 ms
^C
--- 8.8.8.8 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2004ms
rtt min/avg/max/mdev = 18.519/19.824/21.703/1.361 ms
```

```powershell
[nono@node1 ~]$ ping ynov.com
PING ynov.com (104.26.10.233) 56(84) bytes of data.
64 bytes from 104.26.10.233 (104.26.10.233): icmp_seq=1 ttl=55 time=21.4 ms
64 bytes from 104.26.10.233 (104.26.10.233): icmp_seq=2 ttl=55 time=23.1 ms
64 bytes from 104.26.10.233 (104.26.10.233): icmp_seq=3 ttl=55 time=22.8 ms
^C64 bytes from 104.26.10.233: icmp_seq=4 ttl=55 time=22.1 ms

--- ynov.com ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 15180ms
rtt min/avg/max/mdev = 21.379/22.350/23.107/0.664 ms
```

### 3. Serveur Web
---
🌞 Installez le paquet nginx

```
sudo dnf install nginx
```
---
🌞 Créer le site web

```powershell
[nono@web ~]$ cd /var/
[nono@web var]$ sudo mkdir www
[nono@web var]$ cd www
[nono@web www]$ sudo mkdir site_web_nul
[nono@web www]$ cd site_web_nul/
[nono@web site_web_nul]$ sudo touch index.html
[nono@web site_web_nul]$ sudo nano index.html
[nono@web site_web_nul]$ sudo cat index.html
<h1>MEOW <3</h1>
```
---
🌞 Donner les bonnes permissions

> sudo chown -R nginx:nginx /var/www/site_web_nul
---
🌞 Créer un fichier de configuration NGINX pour notre site web

> cd /etc/nginx/conf.d/
```
sudo touch site_web_nul.conf

sudo nano site_web_nul.conf
```

```powershell
[nono@web conf.d]$ sudo cat site_web_nul.conf
server {
  # le port sur lequel on veut écouter
  listen 80;

  # le nom de la page d'accueil si le client de la précise pas
  index index.html;

  # un nom pour notre serveur (pas vraiment utile ici, mais bonne pratique)
  server_name www.site_web_nul.b1;

  # le dossier qui contient notre site web
  root /var/www/site_web_nul;
}
```
---
🌞 Démarrer le serveur web !

> sudo systemctl start nginx

> systemctl status nginx 
```powershell
[nono@web ~]$ systemctl status nginx
● nginx.service - The nginx HTTP and reverse proxy server
     Loaded: loaded (/usr/lib/systemd/system/nginx.service; disabled; preset: disabled)
     Active: active (running) since Tue 2023-11-14 11:42:02 CET; 31s ago
    Process: 1376 ExecStartPre=/usr/bin/rm -f /run/nginx.pid (code=exited, status=0/SUCCESS)
    Process: 1377 ExecStartPre=/usr/sbin/nginx -t (code=exited, status=0/SUCCESS)
    Process: 1378 ExecStart=/usr/sbin/nginx (code=exited, status=0/SUCCESS)
   Main PID: 1379 (nginx)
      Tasks: 2 (limit: 4674)
     Memory: 3.2M
        CPU: 61ms
     CGroup: /system.slice/nginx.service
             ├─1379 "nginx: master process /usr/sbin/nginx"
             └─1380 "nginx: worker process"

Nov 14 11:42:02 web systemd[1]: Starting The nginx HTTP and reverse proxy server...
Nov 14 11:42:02 web nginx[1377]: nginx: the configuration file /etc/nginx/nginx.conf syntax is ok
Nov 14 11:42:02 web nginx[1377]: nginx: configuration file /etc/nginx/nginx.conf test is successful
Nov 14 11:42:02 web systemd[1]: Started The nginx HTTP and reverse proxy server.
```
---
🌞 Ouvrir le port firewall

```powershell
[nono@web ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
success
[nono@web ~]$ sudo firewall-cmd --reload
success
```
---
🌞 Visitez le serveur web !

> http://10.5.1.12

```powershell
[nono@node1 ~]$ curl http://10.5.1.12
<h1>MEOW <3</h1>
```
---
🌞 Visualiser le port en écoute

```
ss -ln | grep ":80"
```

```
tcp   LISTEN 0      511                                       0.0.0.0:80               0.0.0.0:*
tcp   LISTEN 0      511                                          [::]:80                  [::]:*
```
---

🌞 Analyse trafic

