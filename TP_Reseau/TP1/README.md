# TP1 - Premier pas réseau 

## I. Exploration locale en solo

### 1. Affichage d'informations sur la pile TCP/IP locale

---
🌞 Affichez les infos des cartes réseau de votre PC
``` 
ipconfig /all 
```
#### _Interface Wifi :_

- Nom : Intel(R) Wi-Fi 6E AX211 160MHz

- Adresse mac : F4-C8-8A-6F-04-05

- Adresse ip : 10.33.48.106

---

#### _Interface Ethernet :_

- Nom : Realtek PCIe GbE Family Controller

- Adresse MAC : C8-7F-54-CC-00-08

- Adresse IP : pas possible aucune connectique branchée

---
🌞 Affichez votre gateway
#### _Adresse IP passerelle :_

Adresse IP : 10.33.48.1

---
🌞 Déterminer la MAC de la passerelle
```
arp -a
```
#### _Adresse MAC passerelle :_

Adresse MAC : 44-ce-7d-df-99-c0

---
🌞 Trouvez comment afficher les informations sur une carte IP (change selon l'OS)

En allant dans nos paramètres wifi et propiétés sur la connexion actuelle.

>Adresse IPv4 : 10.33.48.106
Serveurs DNS IPv4 :	 10.33.48.1 (non chiffré)
Adresse physique (MAC) :	F4-C8-8A-6F-04-05

### 2. Modifications des informations
---
🌞 Utilisez l'interface graphique de votre OS pour changer d'adresse IP :

Nouvelle IP : 10.33.48.200

Masque de sous réseau : 255.255.255.0

Passerelle : 10.33.48.1

DNS : 8.8.8.8

---
🌞 Il est possible que vous perdiez l'accès internet.

>_Expliquez pourquoi c'est possible de perdre son accès internet en faisant cette opération._

Si l'IP entré manuellement appartient à un autre appareil, il y aura confusion de récéption d'information dans le reseau et donc l'accès aux informations se retrouve bloqué.


## II. Exploration locale en duo

### 3. Modification d'adresse IP
---

🌞 Modifiez l'IP des deux machines pour qu'elles soient dans le même réseau

Nouvelle IP : 10.10.10.11

Masque : 255.255.255.0

---
🌞 Vérifier à l'aide d'une commande que votre IP a bien été changée

```
Avec ipconfig :

Adresse IPv4. . . . . . . . . . . . . .: 10.10.10.11
   Masque de sous-réseau. . . . . . . . . : 255.255.255.0
```
Envoi d'un ping :
```
 ping 10.10.10.12

Envoi d’une requête 'Ping'  10.10.10.12 avec 32 octets de données :
Réponse de 10.10.10.12 : octets=32 temps=5 ms TTL=128
Réponse de 10.10.10.12 : octets=32 temps=5 ms TTL=128
Réponse de 10.10.10.12 : octets=32 temps=2 ms TTL=128
Réponse de 10.10.10.12 : octets=32 temps=3 ms TTL=128

Statistiques Ping pour 10.10.10.12:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 2ms, Maximum = 5ms, Moyenne = 3ms
```

---
🌞 Déterminer l'adresse MAC de votre correspondant

Avec :
```
arp -a
```
Adresse MAC du correspondant :
```
 Adresse Internet      Adresse physique      Type
  10.10.10.12           08-bf-b8-c2-2a-57     dynamique
```
### 4. Petit chat privé
---
🌞 sur le PC client avec par exemple l'IP 192.168.1.2

Client :
```
Cmd line: 10.10.10.12 8888
K
coucou  
```

---
🌞 Visualiser la connexion en cours :
```
 [nc64.exe]
  TCP    10.33.48.106:139       0.0.0.0:0              LISTENING
 Impossible d’obtenir les informations de propriétaire
  TCP    10.33.48.106:49413     20.199.120.182:443     ESTABLISHED
  WpnService
```

### 5. Firewall
---
🌞 Activez et configurez votre pare-feu

On autorise avec les paramètres avancées, on active les règles sur "oui" pour netcat. 

## III. Manipulations d'autres outils/protocoles côté client

### 1. DHCP (partie réalisé chez moi à partir de ma box internet)
---
🌞Exploration du DHCP, depuis votre PC

```
ipconfig /all
```
résultat :
```
 Bail obtenu. . . . . . . . . . . . . . : lundi 16 octobre 2023 13:16:57
   Bail expirant. . . . . . . . . . . . . : mardi 17 octobre 2023 14:29:33
Serveur DHCP . . . . . . . . . . . . . : 192.168.1.1
```
### 2.DNS
---
🌞 Trouver l'adresse IP du serveur DNS que connaît votre ordinateur
```
 Serveurs DNS. . .  . . . . . . . . . . : 192.168.1.1
```

---
🌞Utiliser, en ligne de commande l'outil nslookup(Windows, MacOS) ou dig(GNU/Linux, MacOS) pour faire des requêtes DNS à la main

```
 - nslookup google.com
Serveur :   box
Address:  192.168.1.1

Réponse ne faisant pas autorité :
Nom :    google.com
Addresses:  2a00:1450:4007:813::200e
          216.58.214.174
```
```
 - nslookup ynov.com
Serveur :   box
Address:  192.168.1.1

Réponse ne faisant pas autorité :
Nom :    ynov.com
Addresses:  2606:4700:20::681a:ae9
          2606:4700:20::ac43:4ae2
          2606:4700:20::681a:be9
          172.67.74.226
          104.26.11.233
          104.26.10.233
```
Ces adresses IP sont associées à différents serveurs ou points d'accès pour le nom du domaine entré.

>_Déterminer l'adresse IP du serveur à qui vous venez d'effectuer ces requêtes_

On effectue les requêtes au près du serveur DNS de google donc 8.8.8.8 .

```
nslookup 231.34.113.12
Serveur :   box
Address:  192.168.1.1
*** box ne parvient pas à trouver 231.34.113.12 : Non-existent domain
```

```
 nslookup 78.34.2.17
Serveur :   box
Address:  192.168.1.1

Nom :    cable-78-34-2-17.nc.de
Address:  78.34.2.17
```

Pour voir si des noms de domaines sont associées à différentes adresses ip.

Pour la première il n'y a pas de nom de domaine associé à cette adresse IP mais pour la deuxième oui.

### 3.Wireshark
---
🌞 Utilisez le pour observer les trames qui circulent entre vos deux carte Ethernet. Mettez en évidence :

Voir fichier .pcapng 

Ping sur la passerelle :
```
Frame 7151	119.323007	192.168.1.93	192.168.1.1	ICMP	74	Echo (ping) request id=0x0001, seq=63/16128, ttl=128 (reply in 7152)

>Ethernet II, Src: IntelCor_6f:04:05 (f4:c8:8a:6f:04:05), Dst: Sfr_df:99:c0 (44:ce:7d:df:99:c0)
>Internet Protocol Version 4, Src: 192.168.1.93, Dst: 192.168.1.1
>Internet Control Message Protocol
```

le retour : 
```
Frame 7152	119.324634	192.168.1.1	192.168.1.93	ICMP	74	Echo (ping) reply id=0x0001, seq=63/16128, ttl=64 (request in 7151)

>Ethernet II, Src: Sfr_df:99:c0 (44:ce:7d:df:99:c0), Dst: IntelCor_6f:04:05 (f4:c8:8a:6f:04:05)
>Internet Protocol Version 4, Src: 192.168.1.1, Dst: 192.168.1.93
>Internet Control Message Protocol
```

