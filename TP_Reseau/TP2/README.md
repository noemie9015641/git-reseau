# TP2 - Ethernet, IP et ARP

## I. Configuration de l'adresse IP

🌞 Mettez en place une configuration réseau fonctionnelle entre les deux machines 

Les deux IP choisies :

- 10.10.10.1
- 10.10.10.2

masque : 255.255.255.252

Adresse réseau : 10.10.10.0

Adresse broadcast : 10.10.10.3

---
🌞 Prouvez que la connexion est fonctionnelle entre les deux machines
```
 ping 10.10.10.1
```
```
Envoi d’une requête 'Ping'  10.10.10.1 avec 32 octets de données :
Réponse de 10.10.10.1 : octets=32 temps=2 ms TTL=128
Réponse de 10.10.10.1 : octets=32 temps=2 ms TTL=128
Réponse de 10.10.10.1 : octets=32 temps=2 ms TTL=128
Réponse de 10.10.10.1 : octets=32 temps=2 ms TTL=128

Statistiques Ping pour 10.10.10.1:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 2ms, Maximum = 2ms, Moyenne = 2ms
```

---
🌞 Wireshark it

> Déterminez, grâce à Wireshark, quel type de paquet ICMP est envoyé par ping

- 0		Echo reply (used to ping)
- 8		Echo request (used to ping)


### II. ARP my bro

🌞 Check the ARP table

```
arp -a 
```
_Binome :_
```
Interface : 10.10.10.2 --- 0x12
  Adresse Internet      Adresse physique      Type
  10.10.10.1            08-bf-b8-c2-2a-57     dynamique
```
---
_Gateway :_
```
Interface : 10.10.10.2 --- 0x12
  Adresse Internet      Adresse physique      Type
  10.33.19.254           c8-7Ff-54-cc-00-08     dynamique
```
---
🌞 Manipuler la table ARP

AVANT :
```
arp -a
```
```
Interface : 10.10.10.2 --- 0x12
  Adresse Internet      Adresse physique      Type
  10.10.10.1            08-bf-b8-c2-2a-57     dynamique
  10.10.10.3            ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  224.77.77.77          01-00-5e-4d-4d-4d     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
  255.255.255.255       ff-ff-ff-ff-ff-ff     statique
```
---
APRES :
```
arp -d *
```

```
Interface : 10.10.10.2 --- 0x12
  Adresse Internet      Adresse physique      Type
  224.0.0.22            01-00-5e-00-00-16     statique
  255.255.255.255       ff-ff-ff-ff-ff-ff     statique
```

  ---
🌞 Wireshark it

voir fichier wireshark 

_plus description:_

__la demande :__
Ethernet II, Src: ASUSTekC_cc:00:08 (c8:7f:54:cc:00:08), Dst: Broadcast (ff:ff:ff:ff:ff:ff)
Address Resolution Protocol (request)

__la réponse:__
Ethernet II, Src: ASUSTekC_c2:2a:57 (08:bf:b8:c2:2a:57), Dst: ASUSTekC_cc:00:08 (c8:7f:54:cc:00:08)
Address Resolution Protocol (reply)

### III. DHCP

🌞 Wireshark it